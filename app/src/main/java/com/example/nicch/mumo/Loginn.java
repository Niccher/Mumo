package com.example.nicch.mumo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Loginn extends AppCompatActivity {

    private Context contx=getBaseContext();
    AlertDialog.Builder builder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_loginn);

        //getSupportActionBar().setTitle("Loging");

        final Button lg= (Button) findViewById(R.id.LgBtn);
        final Button sn= (Button) findViewById(R.id.SignUpa);

        final EditText pwd= (EditText) findViewById(R.id.pwdEd);
        final EditText usr= (EditText) findViewById(R.id.usrEd);

        lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usr.getText().toString().equals("") || pwd.getText().toString().equals("")) {
                    builder = new AlertDialog.Builder(Loginn.this);
                    builder.setTitle("Activity Messed..");
                    builder.setMessage("Fill All Fields Nigga....");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertdialog = builder.create();
                    alertdialog.show();
                    Context cnt = Loginn.this;
                    Intent trv =new Intent(getApplicationContext(), Imger.class);
                    trv.putExtra("UsrN",pwd.getText().toString());
                    //cnt.startActivity(trv);
                } else {
                    final ProgressDialog pg=new ProgressDialog(Loginn.this);
                    pg.setMessage("Performing Credentials Compare ...Please wait");

                    pg.show();
                    final String usern = usr.getText().toString();
                    final String passwd = pwd.getText().toString();

                    Response.Listener<String> responselistener = new Response.Listener<String>() {

                        @Override

                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                if (success) {
                                    Context cnt = Loginn.this;
                                    pg.dismiss();
                                    Intent trv =new Intent(getApplicationContext(), Lander.class);
                                    trv.putExtra("UsrN",usern);
                                    cnt.startActivity(trv);
                                    //startActivity(new Intent(Loginn.this, Lander.class));

                                } else {
                                    pg.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Loginn.this);
                                    builder.setMessage("Wrong Credentials");
                                    builder.setNegativeButton("Retry", null);
                                    builder.create();
                                    builder.show();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(Loginn.this,e+"\nError",Toast.LENGTH_SHORT).show();
                            }

                        }
                    };

                    LogAcc LogAc = new LogAcc(usern, passwd, responselistener);
                    RequestQueue queue = Volley.newRequestQueue(Loginn.this);
                    queue.add(LogAc);
                }
            }
        });

        sn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Loginn.this,Regga.class));
            }
        });


    }

}
