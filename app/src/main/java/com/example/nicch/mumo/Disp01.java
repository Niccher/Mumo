package com.example.nicch.mumo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Disp01 extends AppCompatActivity {

    Button bc,ed,COmm;
    Button bac,comme,Apl;
    TextView mp;
    TextView t1,t2,t3,t4,t5,t6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disp01);

        //getSupportActionBar().setTitle("Loging");
        getSupportActionBar().setTitle("Contract Info");

        t1=(TextView) findViewById(R.id.tV1);
        t2=(TextView) findViewById(R.id.tV2);
        t3=(TextView) findViewById(R.id.tV3);
        t4=(TextView) findViewById(R.id.tV4);
        t5=(TextView) findViewById(R.id.tV5);
        t6=(TextView) findViewById(R.id.tV6);

        Bundle bud=getIntent().getExtras();
        if (bud!=null){
            String tt1=bud.getString("Rid");
            //String tt2=bud.getString("Desc");
            t1.setText(tt1);
            t2.setText(tt1);
            t3.setText(tt1);
            t4.setText(tt1);
            t5.setText(tt1);
            t6.setText(tt1);
        }

        bac= (Button) findViewById(R.id.contBac);
        Apl= (Button) findViewById(R.id.contAppl);
        comme= (Button) findViewById(R.id.contComm);

        bac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Disp01.this, ContaList.class));
            }
        });

        Apl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Disp01.this, Apply.class));
            }
        });

        comme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Disp01.this, Mkr.class));
            }
        });
    }
}
