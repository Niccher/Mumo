package com.example.nicch.mumo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by nicch on 1/6/18.
 */

public class ProjList2 extends RecyclerView.Adapter<ProjList2.ViewHolder> {

    private List<Listem> lisIts;
    private Context cnt;

    public ProjList2(List<Listem> lisIts, Context cnt) {
        this.lisIts = lisIts;
        this.cnt = cnt;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vw= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.panol3,parent,false);
        return new ViewHolder(vw);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Listem lit=lisIts.get(position);
        holder.Ttl.setText("Contract Title - "+lit.getAA());
        holder.Pown.setText("Contract ID - "+lit.getBB());
        holder.Pstat.setText(lit.getDD()+" Kshs");
        holder.Ptyp.setText("Contract Duration (Wks)- "+lit.getCC());
        holder.Pdur.setText(lit.getEE());

        holder.RelYt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(cnt,"You Pressed Count "+position,Toast.LENGTH_LONG).show();
                Intent Goo=new Intent(cnt,Apply.class);
                final String ide=lit.getBB();
                final String ide1=lit.getAA();;;

                Goo.putExtra("pid",ide);
                Goo.putExtra("nam",ide1);

                cnt.startActivity(Goo);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lisIts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView Ttl,Pown,Pdur,Ptyp,Pstat,Pinp,Paut,Pobj;

        public RelativeLayout RelYt;

        public ViewHolder(View itemView) {
            super(itemView);

            Ttl=itemView.findViewById(R.id.PrTt);
            Pown=itemView.findViewById(R.id.PrOwn);
            Pdur=itemView.findViewById(R.id.PrObj);

            Ptyp=itemView.findViewById(R.id.PrDurTy);

            Pstat=itemView.findViewById(R.id.PrStat);
            Pinp=itemView.findViewById(R.id.PrExp);

            RelYt=(RelativeLayout) itemView.findViewById(R.id.belg);
        }
    }
}

