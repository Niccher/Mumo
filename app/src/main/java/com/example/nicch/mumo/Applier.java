package com.example.nicch.mumo;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicch on 9/28/17.
 */

public class Applier extends StringRequest {
    private static final String Writa_Url="https://muruakyone.000webhostapp.com/Brk03/AddApplicant.php";
    //private static final String Writa_Url="http://192.168.42.110/Brk02/AddApplicant.php";
    private Map<String, String> params;

    public Applier(String nms, int Phon, String eml, String prgTt, int ide, String Quall, Response.Listener<String> listener)
        {
        super(Method.POST,Writa_Url,listener,null);
        params=new HashMap<>();
        params.put("Name",nms);
        params.put("PhoneNo",Phon+"");
        params.put("Email",eml);
        params.put("ProjTitle",prgTt);
        params.put("ProjID",ide+"");
        params.put("Qualifications",Quall);

    }


    public Map<String, String> getParams()
    {
        return params;
    }
}
