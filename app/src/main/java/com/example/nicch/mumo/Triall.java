package com.example.nicch.mumo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Triall extends AppCompatActivity {
    TextView t1,t2,t3,t4,t5,t6;
    Button bc,comm,apl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_trial);

        getSupportActionBar().setTitle("Awarded Contracts");

        t1=(TextView) findViewById(R.id.tv1);
        t2=(TextView) findViewById(R.id.tv2);
        t3=(TextView) findViewById(R.id.tV3);
        t4=(TextView) findViewById(R.id.tv4);
        t5=(TextView) findViewById(R.id.tV5);
        t6=(TextView) findViewById(R.id.tv6);

        bc=(Button) findViewById(R.id.contBac);

        comm=(Button) findViewById(R.id.contComm);
        apl=(Button) findViewById(R.id.contAppl);

        comm.setEnabled(Boolean.FALSE);
        apl.setEnabled(Boolean.FALSE);

        Bundle bud=getIntent().getExtras();
        if (bud!=null){
            String tt1=bud.getString("Rid");
            String tt2=bud.getString("Desc");
            String tt3=bud.getString("ContDur");
            String tt4=bud.getString("ContVal");
            String tt5=bud.getString("Aw");
            String tt6=bud.getString("ContID");
            t1.setText(tt1);
            t2.setText(tt2);
            t3.setText(tt3);
            t4.setText(tt4);
            t5.setText(tt5);
            t6.setText(tt6);
        }

        bc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Triall.this,AwContra.class));
            }
        });
    }
}
