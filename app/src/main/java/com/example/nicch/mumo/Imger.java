package com.example.nicch.mumo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Imger extends AppCompatActivity implements View.OnClickListener{

    private Button sel, sed;
    private ImageView seeit;
    private EditText nm,coid,conm;

    private final int img=1;
    private int conp = 0;
    private Bitmap bmp;

    //private static final String Senda="http://192.168.42.110/Brk02/uploading.php";
    private static final String Senda="https://muruakyone.000webhostapp.com/Brk03/Uploading.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_imger);

        getSupportActionBar().setTitle("Documents To Upload");

        sel= (Button) findViewById(R.id.btn_chos);
        sed= (Button) findViewById(R.id.btn_sed);

        seeit= (ImageView) findViewById(R.id.imghr);
        nm= (EditText) findViewById(R.id.etName);

        coid= (EditText) findViewById(R.id.etContID);
        conm= (EditText) findViewById(R.id.etContNM);

        Bundle bud=getIntent().getExtras();
        if (bud!=null){
            final String val=bud.getString("ContID");
            final String val1=bud.getString("ContNM");
            conp=bud.getInt("ContAPP");
            conm.setText("Contract Name "+val1);
            coid.setText("Contract ID "+val);
        }

        sel.setOnClickListener(this);
        sed.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_chos:
                GettaImg();
                break;
            case R.id.btn_sed:
                Seda();
                break;
        }
    }

    private void GettaImg(){
        Intent intt=new Intent();
        intt.setType("image/*");
        intt.setAction(intt.ACTION_GET_CONTENT);
        startActivityForResult(intt,img);

    }

    private void Seda(){
        final ProgressDialog pg=new ProgressDialog(Imger.this);
        pg.setMessage("Uploading Image To Remote Location ...Please wait");
        pg.show();
        StringRequest strss=new StringRequest(Request.Method.POST, Senda, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    pg.dismiss();
                    JSONObject job=new JSONObject(response);
                    String ress=job.getString("respons");
                    //Toast.makeText(Imger.this, "Outcome"+ress, Toast.LENGTH_SHORT).show();
                    seeit.setImageResource(0);
                    seeit.setVisibility(View.GONE);
                    startActivity(new Intent(Imger.this,Lander.class));
                } catch (JSONException e) {
                    pg.dismiss();
                    Toast.makeText(Imger.this, "This Error "+e, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pg.dismiss();
                Toast.makeText(Imger.this, "This Volley-Error "+error, Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param =new  HashMap<>();
                param.put("Name",nm.getText().toString());
                param.put("ContractID",coid.getText().toString());
                param.put("Applicant",String.valueOf(conp));
                param.put("Image",imageToString(bmp));
                return param;
            }
        };

        Oncess.getInstance(Imger.this).addToRequestQue(strss);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==img && resultCode==RESULT_OK && data!=null){
            Uri pat=data.getData();
            try {
                bmp= MediaStore.Images.Media.getBitmap(getContentResolver(),pat);
                seeit.setVisibility(View.VISIBLE);
                nm.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                Toast.makeText(Imger.this, "Bitmap Error "+e, Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private String imageToString(Bitmap bmp){
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgBytes=byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes,Base64.DEFAULT);
    }
}
