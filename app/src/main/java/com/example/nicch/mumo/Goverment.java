package com.example.nicch.mumo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Goverment extends AppCompatActivity {

    private RecyclerView rcVw;
    private  RecyclerView.Adapter RcVwAd;

    private List<List2> listITs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_goverment);

        getSupportActionBar().setTitle("Goverment Feeds");

        rcVw= (RecyclerView) findViewById(R.id.Recv);
        rcVw.setHasFixedSize(true);
        rcVw.setLayoutManager(new LinearLayoutManager(this));

        listITs=new ArrayList<>();


        RcVwAd =new Gvt(listITs,Goverment.this);

        rcVw.setAdapter(RcVwAd);
    }
}
