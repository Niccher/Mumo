package com.example.nicch.mumo;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by nicch on 1/24/18.
 */

public class Oncess {
    private static Oncess onb;
    private RequestQueue req;
    private static Context cnt;

    private Oncess(Context conn){
        cnt=conn;
        req=getRequestQueus();
    }

    private RequestQueue getRequestQueus() {
        if(req==null)
            req= Volley.newRequestQueue(cnt.getApplicationContext());
        return req;
    }

    public static synchronized Oncess getInstance(Context context){
        if (onb==null){
            onb=new Oncess(context);
        }
        return onb;
    }

    public <T> void addToRequestQue(Request<T> request){
        getRequestQueus().add(request);
    }
}
