package com.example.nicch.mumo;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by nicch on 12/9/17.
 */

public class AdptaAw extends RecyclerView.Adapter<AdptaAw.ViewHolder> {

    public AdptaAw(List<Listem> lisIts, Context cnt) {
        this.lisIts = lisIts;
        this.cnt = cnt;
    }

    private List<Listem> lisIts;
    private Context cnt;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vw = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.panolaw, parent, false);
        return new ViewHolder(vw);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Listem lit = lisIts.get(position);
        holder.Aw.setText(lit.getAA());
        holder.CVal.setText(lit.getBB());
        holder.CDur.setText(lit.getCC());
        holder.CDesc.setText(lit.getDD());
        holder.CID.setText(lit.getEE());
        int ff=holder.getAdapterPosition();
        final String kk= (String) holder.CDesc.getText();
        final String kk1= (String) holder.CID.getText();
        final String kk2= (String) holder.CDur.getText();
        final String kk3= (String) holder.CVal.getText();
        final String kk4= (String) holder.Aw.getText();
        final String fff=String.valueOf(ff);
        getItemCount();

        holder.RelY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mov = new Intent(cnt, Triall.class);
                //String Rid=null;
                mov.putExtra("Rid",fff);
                mov.putExtra("Desc",kk);
                mov.putExtra("ContID",kk1);
                mov.putExtra("ContDur",kk2);
                mov.putExtra("ContVal",kk3);
                mov.putExtra("Aw",kk4);
                //Toast.makeText(holder,"",Toast.LENGTH_LONG).show();
                cnt.startActivity(mov);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lisIts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView Aw, CVal,CDur,CDesc,CID;
        public ImageView Imv;

        public RelativeLayout RelY;

        public ViewHolder(View itemView) {
            super(itemView);

            RelY = (RelativeLayout) itemView.findViewById(R.id.noggOrda);

            Aw = itemView.findViewById(R.id.AwaTo);
            CVal = itemView.findViewById(R.id.ContaVal);
            CDur = itemView.findViewById(R.id.ContaDur);
            CDesc = itemView.findViewById(R.id.ContaDesc);
            CID = itemView.findViewById(R.id.ContaID);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    //Snackbar.make(view, "Preparing Data " + pos, Snackbar.LENGTH_LONG).setAction("Action Snack", null).show();
                    //startActivity(new Intent(cnt, Heada.class));

                }
            });

        }
    }
}
