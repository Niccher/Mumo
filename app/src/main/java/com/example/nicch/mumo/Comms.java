package com.example.nicch.mumo;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicch on 10/18/17.
 */

public class Comms extends StringRequest {
    private static final String Writa_Url="https://muruakyone.000webhostapp.com/Brk03/Commenta.php";
    private Map<String, String> params;
    //nms,eml,com,cid,uid
    public Comms(String nms, String eml, String Comn,String cid, int IdNo, Response.Listener<String> listener)
    {
        super(Method.POST,Writa_Url,listener,null);
        params=new HashMap<>();
        params.put("Name",nms);
        params.put("Email",eml);
        params.put("Comment",Comn);
        params.put("UID",IdNo+"");
        params.put("Dat",cid);

    }


    public Map<String, String> getParams()
    {
        return params;
    }
}
