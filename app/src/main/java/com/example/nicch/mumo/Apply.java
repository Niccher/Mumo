package com.example.nicch.mumo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Apply extends AppCompatActivity {

    Button apl;
    AlertDialog.Builder Onyo;
    private Context cont=getBaseContext();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_apply);

        getSupportActionBar().setTitle("Apply Contract");

        final EditText nm= (EditText) findViewById(R.id.etNam);
        final EditText pnn= (EditText) findViewById(R.id.etPNuber);
        final EditText prjtt= (EditText) findViewById(R.id.etPrjTT);
        final EditText prjid= (EditText) findViewById(R.id.etPrjID);
        final EditText prjqual= (EditText) findViewById(R.id.etQual);
        final EditText preml= (EditText) findViewById(R.id.etEml);

        Bundle bud=getIntent().getExtras();
        if (bud!=null){
            final String val=bud.getString("pid");
            final String val1=bud.getString("nam");
            prjtt.setText(val1);
            prjid.setText(val);
        }

        apl= (Button) findViewById(R.id.Aply);

        apl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nm.getText().toString().equals("")||pnn.getText().toString().equals("")||preml.getText().toString().equals("")||prjid.getText().toString().equals("")||prjtt.getText().toString().equals("")||prjqual.getText().toString().equals("")){
                    Onyo=new AlertDialog.Builder(Apply.this);
                    Onyo.setTitle("Blanks Left..");
                    Onyo.setMessage("Fill Blank Fields...");
                    Onyo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog AllDg=Onyo.create();
                    AllDg.show();
                }
                else{
                    final ProgressDialog prg=new ProgressDialog(Apply.this);
                    prg.setMessage("Processing Request...Please wait");

                    prg.show();
                    final int Phon= Integer.parseInt(pnn.getText().toString());
                    final String nms=nm.getText().toString();
                    final String eml=preml.getText().toString();
                    final String prgTT=prjtt.getText().toString();
                    final int idd=Integer.parseInt(prjid.getText().toString());
                    final String Qual=prjqual.getText().toString();

                    Response.Listener<String> responseListener=new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse=new JSONObject(response);
                                boolean success=jsonResponse.getBoolean("success");
                                    if(success){
                                        Context cnt=Apply.this;
                                        prg.dismiss();
                                        Intent ingo=new Intent(getApplicationContext(),Imger.class);
                                        ingo.putExtra("ContID",prjid.getText().toString());
                                        ingo.putExtra("ContNM",prjtt.getText().toString());
                                        ingo.putExtra("ContAPP", Phon);
                                        //Onyo=new AlertDialog.Builder(Apply.this);
                                        //Onyo.setMessage("Application Succesfull");
                                        cnt.startActivity(ingo);
                                    }
                                    else {
                                        prg.dismiss();
                                        Onyo=new AlertDialog.Builder(Apply.this);
                                        Onyo.setMessage("Application Was Unsuccessfull");
                                        Onyo.setNegativeButton("Retry",null);
                                        Onyo.create();
                                        Onyo.show();
                                    }
                            } catch (JSONException e) {
                                Toast.makeText(getApplicationContext(), "Application Error "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }

                        }
                    };
                    Applier RcONN=new Applier(nms,Phon,eml,prgTT,idd,Qual,responseListener);
                    RequestQueue queue= Volley.newRequestQueue(Apply.this);
                    queue.add(RcONN);
                }
            }
        });


    }
}
