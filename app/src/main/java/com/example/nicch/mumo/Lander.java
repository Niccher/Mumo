package com.example.nicch.mumo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Lander extends AppCompatActivity {

    Button vc,gfed,rats,pstCont;
    Context cntt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activ_lander);

        gfed= (Button) findViewById(R.id.gFeeds);

        pstCont=(Button) findViewById(R.id.awCont);

        vc= (Button) findViewById(R.id.VwContr);

        rats= (Button) findViewById(R.id.RatUs);

        Bundle bud=getIntent().getExtras();
            //final String tt1=bud.getString("UsrN");

        vc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Lander.this,"Action Pending",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Lander.this, ContaList.class));
            }
        });

        rats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent trv =new Intent(Lander.this, Commenta.class);
                //trv.putExtra("UsrN",tt1);
                //cntt.startActivity(trv);
                startActivity(new Intent(Lander.this, Commenta.class));
            }
        });

        pstCont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Lander.this, AwContra.class));
            }
        });

        gfed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Lander.this,"Action Pending",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Lander.this, Goverment.class));

            }
        });
    }
}
