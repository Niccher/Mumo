package com.example.nicch.mumo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Commenta extends AppCompatActivity {

    EditText mun,val1,val2,val3,val4;
    Button com;
    Calendar cal=new GregorianCalendar();
    AlertDialog.Builder Onyo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_commenta);

        getSupportActionBar().setTitle("Comment");

        val1= (EditText) findViewById(R.id.etNam);
        val2= (EditText) findViewById(R.id.etEml);
        val3= (EditText) findViewById(R.id.etComm);
        //val4= (EditText) findViewById(R.id.etCont);

        int dy=cal.get(Calendar.DAY_OF_MONTH);
        int dy2=cal.get(Calendar.MONTH)+1;
        int dy3=cal.get(Calendar.YEAR);

        mun= (EditText) findViewById(R.id.edMun);
        com= (Button) findViewById(R.id.btnSav);

        mun.setEnabled(Boolean.FALSE);

        Bundle bud=getIntent().getExtras();
        if (bud!=null){
            //String tt1=bud.getString("UsrN");
            //val1.setText(tt1);
        }

        mun.setText(String.valueOf(dy)+"--"+String.valueOf(dy2)+"--"+String.valueOf(dy3));

        final String dyt=(String.valueOf(dy)+"--"+String.valueOf(dy2)+"--"+String.valueOf(dy3));

        com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String var1,var2,var3,var4;

                var1=String.valueOf(val1.getText());
                var2=String.valueOf(val2.getText());
                var3=String.valueOf(val3.getText());
                //var4=String.valueOf(val4.getText());

                if(var1.equals("")||var1.equals("")||var2.equals("")||var3.equals("")/*||var4.equals("")*/){
                    Onyo=new AlertDialog.Builder(Commenta.this);
                    Onyo.setTitle("Blanks..");
                    Onyo.setMessage("Please fill all Comment fields....");
                    Onyo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog AllDg=Onyo.create();
                    AllDg.show();
                }
                else{
                    final ProgressDialog progressDialog=new ProgressDialog(Commenta.this);
                    progressDialog.setMessage("Inserting To Comments..Please wait");
                    progressDialog.show();

                    final String nms=var1;
                    final String eml=var2;
                    final String com=var3;
                    final String cid=dyt;/*Integer.parseInt(String.valueOf(var4))*/;
                    final int uid=Integer.parseInt(String.valueOf("54"));

                    Response.Listener<String> responseListener=new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse=new JSONObject(response);
                                boolean success=jsonResponse.getBoolean("success");
                                    if(success){
                                        progressDialog.dismiss();
                                        Onyo=new AlertDialog.Builder(Commenta.this);
                                        Onyo.setMessage("Your Comment has Been Succesfully Parsed");
                                        //Onyo.setPositiveButton("Continue",null);
                                        Onyo.setPositiveButton("Continue",new DialogInterface.OnClickListener(){

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                val1.setText(null);
                                                val2.setText(null);
                                                val3.setText(null);

                                                startActivity(new Intent(Commenta.this,Lander.class));
                                            }
                                        });
                                        Onyo.create();
                                        Onyo.show();
                                    }
                                    else {
                                        progressDialog.dismiss();
                                        Onyo=new AlertDialog.Builder(Commenta.this);
                                        Onyo.setMessage("An Expected Error Occured");
                                        Onyo.setNegativeButton("Retry",null);
                                        Onyo.create();
                                        Onyo.show();
                                    }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(Commenta.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        }
                        public void onErrorResponse(VolleyError er) {
                            Toast.makeText(Commenta.this, er.getMessage(), Toast.LENGTH_LONG).show();
                            VolleyLog.e("Error Hahaha: ", er.getMessage());
                        }
                    };
                    Comms comb=new Comms(nms,eml,com,cid,uid,responseListener);
                    RequestQueue queue= Volley.newRequestQueue(Commenta.this);
                    queue.add(comb);
                }
            }
        });
    }
}
