package com.example.nicch.mumo;

import android.app.Activity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GoodCont extends Activity {
    private RecyclerView rcVw;
    private  RecyclerView.Adapter RcVwAd;

    private String pat="https://muruakyone.000webhostapp.com/Brk03/Lista.php";
    private List<Listem> listITs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_cont);

        rcVw= (RecyclerView) findViewById(R.id.MyDropa);
        rcVw.setHasFixedSize(true);
        rcVw.setLayoutManager(new LinearLayoutManager(this));

        listITs=new ArrayList<>();

        Serc();

        RcVwAd =new ProjList(listITs,GoodCont.this);

        rcVw.setAdapter(RcVwAd);
    }

    private void Serc(){
        final ProgressDialog progressDialog=new ProgressDialog(GoodCont.this);
        progressDialog.setMessage("Performing Fetch Action..Please wait");
        progressDialog.show();

        StringRequest stringRequest=new StringRequest(Request.Method.GET, pat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONObject jsonObject=new JSONObject(response);

                    JSONArray array=jsonObject.getJSONArray("Andu");
                    for(int i=0;i<array.length();i++)
                    {
                        JSONObject jobb=array.getJSONObject(i);
                        Listem lita=new Listem(
                                jobb.getString("Name"),
                                jobb.getString("ContractID"),
                                jobb.getString("Durationn"),
                                jobb.getString("Cost"),
                                jobb.getString("Requirements"),
                                jobb.getString("Award")
                        );
                        listITs.add(lita);

                    }
                    RcVwAd=new ProjList(listITs,GoodCont.this);

                    rcVw.setAdapter(RcVwAd);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(GoodCont.this,error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue= Volley.newRequestQueue(GoodCont.this);
        requestQueue.add(stringRequest);
    }

}
