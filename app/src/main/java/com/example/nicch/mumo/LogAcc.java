package com.example.nicch.mumo;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicch on 10/18/17.
 */

public class LogAcc extends StringRequest {
    private static final String Log_Path="https://muruakyone.000webhostapp.com/Brk03/Loginn.php";
    //private static final String Log_Path="http://192.168.42.110/Brk02/Loginn.php";
    private Map<String, String> params;

    public LogAcc(String username, String password, Response.Listener<String> listener)
    {
        super(Method.POST,Log_Path,listener,null);
        params=new HashMap<>();
        params.put("Username",username);
        params.put("Password",password);


    }
    public Map<String, String> getParams()
    {
        return params;
    }
}
