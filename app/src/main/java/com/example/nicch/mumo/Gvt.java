package com.example.nicch.mumo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by nicch on 2/26/18.
 */

public class Gvt extends RecyclerView.Adapter<Gvt.ViewHolder> {

    public Gvt(List<List2> lisIts, Context cnt) {
        this.lisIts = lisIts;
        this.cnt = cnt;
    }

    private List<List2> lisIts;
    private Context cnt;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vw = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.govfeed, parent, false);
        return new ViewHolder(vw);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        List2 lit = lisIts.get(position);
        holder.tvHd.setText(lit.getAA());
        holder.tvTl.setText(lit.getBB());

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvHd, tvTl;

        public RelativeLayout RelY;
        public ViewHolder(View itemView) {
            super(itemView);

            RelY = (RelativeLayout) itemView.findViewById(R.id.grad);

            tvHd = itemView.findViewById(R.id.hed);
            tvTl = itemView.findViewById(R.id.tail);
        }
    }
}
