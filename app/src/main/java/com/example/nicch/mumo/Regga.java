package com.example.nicch.mumo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Regga extends AppCompatActivity {

    Button wrt;
    AlertDialog.Builder Onyo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_regga);

        getSupportActionBar().setTitle("Create Account");

        wrt= (Button) findViewById(R.id.Signpe);

        final EditText nm= (EditText) findViewById(R.id.etNam);
        final EditText pnn= (EditText) findViewById(R.id.etPNuber);
        final EditText Uns= (EditText) findViewById(R.id.etPrjTT);
        final EditText Pw= (EditText) findViewById(R.id.etPrjID);
        final EditText ml= (EditText) findViewById(R.id.etEml);
        final EditText Idn= (EditText) findViewById(R.id.etId);

        pnn.requestFocus();

        wrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nm.getText().toString().equals("")||pnn.getText().toString().equals("")||Uns.getText().toString().equals("")||Pw.getText().toString().equals("")||ml.getText().toString().equals("")){
                    Onyo=new AlertDialog.Builder(Regga.this);
                    Onyo.setTitle("BLank Spaces Left..");
                    Onyo.setMessage("Please fill all the fields....");
                    Onyo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog AllDg=Onyo.create();
                    AllDg.show();
                }
                else{
                    final int Phon= Integer.parseInt(pnn.getText().toString());
                    final String nms=nm.getText().toString();
                    final String eml=ml.getText().toString();
                    final String Usrn=Uns.getText().toString();
                    final String pwdd=Pw.getText().toString();
                    final int Idnn=Integer.parseInt(Idn.getText().toString());

                    final ProgressDialog pdia=new ProgressDialog(Regga.this);
                    pdia.setMessage("Connecting to Servers...Please wait");

                    pdia.show();

                    Response.Listener<String> responseListener=new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse=new JSONObject(response);
                                boolean success=jsonResponse.getBoolean("success");
                                if(success){
                                    pdia.dismiss();
                                    startActivity(new Intent(Regga.this,Loginn.class));
                                    Toast.makeText(Regga.this,"Your Username :"+Usrn+"\nPassword :"+pwdd,Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    pdia.dismiss();
                                    AlertDialog.Builder builder=new AlertDialog.Builder(Regga.this);
                                    builder.setMessage("Registration failed");
                                    builder.setNegativeButton("Retry",null);
                                    builder.create();
                                    builder.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(Regga.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        public void onErrorResponse(VolleyError er) {
                            Toast.makeText(Regga.this, er.getMessage(), Toast.LENGTH_SHORT).show();
                            VolleyLog.e("Error Hahaha: ", er.getMessage());
                        }
                    };
                    HahaNew RcONN=new HahaNew(nms,eml,Idnn,Phon,Usrn,pwdd,responseListener);
                    RequestQueue queue= Volley.newRequestQueue(Regga.this);
                    queue.add(RcONN);
                }
            }
        });
    }
}
