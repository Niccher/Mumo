package com.example.nicch.mumo;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class Adpta extends RecyclerView.Adapter<Adpta.ViewHolder> {

        public Adpta(List<Listem> lisIts, Context cnt) {
            this.lisIts = lisIts;
            this.cnt = cnt;
        }

        private List<Listem> lisIts;
        private Context cnt;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View vw = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.panol, parent, false);
            return new ViewHolder(vw);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            Listem lit = lisIts.get(position);
            holder.tvHd.setText(lit.getAA());
            holder.tvHd2.setText(lit.getDD());
            //int ff=lit.getInpid();
            getItemCount();
            int ff=holder.getAdapterPosition();
            final String fff=String.valueOf(ff);
            getItemCount();

            /*Picasso.with(cnt)
                    .load(lit.Imgurl())
                    .into(holder.Imv);*/

            holder.RelY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mov = new Intent(cnt, Disp01.class);
                    mov.putExtra("Rid",fff);
                    cnt.startActivity(mov);
                }
            });
        }

        @Override
        public int getItemCount() {
            return lisIts.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView tvHd, tvHd2;
            public ImageView Imv;

            public RelativeLayout RelY;

            public ViewHolder(View itemView) {
                super(itemView);

                RelY = (RelativeLayout) itemView.findViewById(R.id.noggOrda);

                tvHd = itemView.findViewById(R.id.Lab);
                tvHd2 = itemView.findViewById(R.id.Lab2);
                Imv = itemView.findViewById(R.id.ImLab);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = getAdapterPosition();
                        Snackbar.make(view, "Preparing Data " + pos, Snackbar.LENGTH_LONG).setAction("Action Snack", null).show();
                        //startActivity(new Intent(cnt, Heada.class));

                    }
                });

            }
        }
   }
